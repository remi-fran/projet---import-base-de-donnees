createSIV = """create table SIV (
    address_titulaire TEXT NOT NULL,
    nom TEXT NOT NULL,
    immatriculation TEXT primary key,
    date_immatriculation TEXT NOT NULL,
    vin INTEGER NOT NULL,
    marque TEXT NOT NULL,
    denomination_commerciale TEXT NOT NULL,
    couleur TEXT NOT NULL,
    carrosserie TEXT NOT NULL,
    categorie TEXT NOT NULL,
    cylindree INTEGER NOT NULL,
    energy INTEGER NOT NULL,
    places INTEGER NOT NULL,
    poids REAL NOT NULL,
    puissance INTEGER NOT NULL,
    type TEXT NOT NULL,
    variante TEXT NOT NULL,
    version INTEGER NOT NULL
);"""

updateSIV = """UPDATE SIV SET address_titulaire = :address_titulaire, nom = :nom, prenom = :prenom, 
immatriculation = :immatriculation, date_immatriculation = :data_immatriculation, vin = :vin, marque = :marque,
denomination_commerciale = :denomination_commerciale, couleur = :couleur, carrosserie = :carrosserie, categorie = :categorie,
cylindree = :cylindree, energy = :energy, places = :places, poids = :poids, puissance = :puissance, type = :type,
variante = :variante, version = :version WHERE immatriculation = :immatriculation;"""

insertSIV = """INSERT into SIV
values (:address_titulaire, :nom, :prenom, :immatriculation, :date_immatriculation, :vin, :marque, :denomination_commerciale,
:couleur, :carrosserie, :categorie, :cylindree, :energie, :places, :poids, :puissance, :type, :variante, :version )
"""

    
