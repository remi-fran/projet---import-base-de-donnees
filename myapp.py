import logging

def SIVExist(cursor):
    #Teste si la table SIV existe
    try:
        cursor.execute("SELECT * FROM SIV")
        return True
    except:
        cursor.executescript(createSIV)
        logging.info("Table SIV créée")
        return False

def readFile(file, delimiter):
    #Lit le fichier passé en paramètre
    liste = []
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter)
        for row in reader:
            liste.append(row)
    return liste

def dataExists(immat, cursor):
    #Teste si les donnéees existent dans la BDD
    cursor.execute("SELECT COUNT(*) FROM SIV WHERE immatriculation = ?", immat)
    if (cursor.fetchone() == (0,)):
        return False
    else:
        return True
    
def insertData(data, cursor):
    #Insere les données dans la BDD
    try:
        cursor.execute(insertSIV, data)
        logging.debug('Mise a jour effectuée')
    except sqlite.Error:
        logging.error('Insérer une erreur')

def updateData(data, cursor):
    #Met a jour les données dans la BDD
    try:
        cursor.execute(updateSIV, data)
        logging.debug('Insertion effectuée')
    except sqlite.Error:
        logging.error('Mise a jour de l erreur')

def main():
    logging.basicConfig(filename='myapp.log',level=logging.INFO)
    logging.info('Started')
    logging.info('Finished')

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Mise a jour de la base de données avec les données du fichier CSV")
    parser.add_argument("data_file", help = "Le fichier de données")
    args = parser.parse_args()
    logging.basicConfig(filename="import.log", level = logging.DEBUG)
    connection = sqlite3.connect('file.sqlite3')
    logging.info('Connection à la base de données')
    cursor = connection.cursor()
    datas = readFile(args.data_file, ';')
    SIVExist(cursor)
    for data in datas
        immat = (data['immatriculation'],)
        if (dataExists(immat, cursor)):
            updateData(data, cursor)
        else:
            insertData(data, cursor)
    connection.commit()
    connection.close()